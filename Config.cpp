#include "Config.h"
#include <iostream>

using namespace std;

Config::Config() {
	setWindowX(16);
	setWindowY(16);
	setOverlapX(0);
	setOverlapY(0);
	setScaleX(1.0);
	setScaleY(1.0);
	setDt(1.0);
}

/*
*	GETTER METHODS
*/

int Config::getWindowX() {
	return windowX;
}
	
int Config::getWindowY() {
	return windowY;
}

int Config::getOverlapX() {
	return overlapX;
}

int Config::getOverlapY() {
	return overlapY;
}

double Config::getScaleX() {
	return scaleX;
}
double Config::getScaleY() {
	return scaleY;
}
double Config::getDt() {
	return dt;
}

/*
*	SETTER METHODS
*/
void Config::setWindowX(int x) {
	windowX = x;
}
	
void Config::setWindowY(int x) {
	windowY = x;
}

void Config::setOverlapX(int x) {
	overlapX = x;
}

void Config::setOverlapY(int x) {
	overlapY = x;
}

void Config::setScaleX(double d) {
	scaleX = d;
}
void Config::setScaleY(double d) {
	scaleY = d;
}
void Config::setDt(double d) {
	dt = d;
}

/*
*	DISPLAY ALL CONFIG INFO
*/
void  Config::printConfig() {
	cout << "Interrogation window size: " << getWindowX() << "x" << getWindowY() << endl;
	cout << "Window overlap: x = " << getOverlapX() << "pix, y = " << getOverlapY() << "pix" << endl;
	cout << "Image scale x: 1pix = " << getScaleX() << "mm" << endl;
	cout << "Image scale y: 1pix = " << getScaleY() << "mm" << endl;
	cout << "dt = " << getDt() << endl << endl;
}