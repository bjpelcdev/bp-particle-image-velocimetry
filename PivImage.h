#ifndef PIVIMAGE_H
#define PIVIMAGE_H

#include <opencv2/opencv.hpp>
#include <string>


class PivImage {

	public:
		// Constructors
		PivImage(std::string fileName);

		// Getter methods
		int getWidth();
		int getHeight();
		int getPixel(int row, int col);

		// Setter methods

		// Print image info
		void info();
		void printImage();

	private:
		cv::Mat image;

};

#endif // PIVIMAGE_H