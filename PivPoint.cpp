#include "PivPoint.h"
#include "XCorr2.h"
#include <iostream>

PivPoint::PivPoint() {

}

int PivPoint::getX() {
	return i_x;
};

int PivPoint::getY() {
	return j_y;
};

double PivPoint::getU() {
	return u;
};

double PivPoint::getV() {
	return v;
};

XCorr2 PivPoint::getCCF() {
	return ccf;
}


// Setters
void PivPoint::setX(int x) {
	i_x = x;
};

void PivPoint::setY(int y) {
	j_y = y;
};

void PivPoint::setU(double uVel) {
	u = uVel;
};

void PivPoint::setV(double vVel) {
	v = vVel;
};

void PivPoint::setCCF(XCorr2 ccfObj) {
	ccf = ccfObj;
}

// print point
void PivPoint::printPoint() {
	std::cout << getX() << "\t" << getY() << "\t" << getU() << "\t" << getV() << "\n";
}
