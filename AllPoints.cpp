#include "PivPoint.h"
#include "AllPoints.h"
#include <iostream>
#include <fstream>

AllPoints::AllPoints() {

}

void AllPoints::addPoint(PivPoint point) {
	allPoints.push_back(point);
}

void AllPoints::printPoints() {
	for (PivPoint pt : allPoints)
		pt.printPoint();
}

void AllPoints::printToFile() {
	// declare output file stream
	std::ofstream outputFile;
	outputFile.open("output2.txt");

	for (PivPoint pt : allPoints) {
		if (!isnan(pt.getU()) && !isnan(pt.getV()))
			outputFile << pt.getX() << "\t" << pt.getY() << "\t" << pt.getU() << "\t" << pt.getV() << "\n";
	}

	outputFile.close();
	return;
}