#include "PivImage.h"
#include <iostream>
//#include <Eigen/Dense>
#include <opencv2/opencv.hpp>
#include <string>

using namespace std;
using namespace cv;

PivImage::PivImage(string fileName) {

    image = imread( fileName, 0 );

    if ( !image.data )
    {
    	cout << "Error: no image data" << endl;
    }

    //namedWindow("Display Image", CV_WINDOW_AUTOSIZE );
    //imshow("Display Image", image);
    //waitKey(0);
}

int PivImage::getWidth() {
	return image.cols;
}

int PivImage::getHeight() {
	return image.rows;
}

void PivImage::info() {
	cout << "Image dimensions (pix x pix): " << getWidth() << " x " << getHeight() << endl << endl;
}

void PivImage::printImage() {
	Scalar intensity;
	
	for (int row = 0; row < getHeight(); ++row) {
		for (int col = 0; col < getWidth(); ++col) {
			intensity = image.at<uchar>(row, col);
			cout << intensity.val[0] << "\t";
		}
		cout << endl;
	}
}

int PivImage::getPixel(int row, int col) {
	if (row > getHeight() || col > getWidth()) {
		cout << "ERROR: pixel coordinate (" << row << ", " << col << "is out of bounds" << endl;
		return -1;
	}
	else {
		return ((Scalar) image.at<uchar>(row, col)).val[0];
	}
}

