#ifndef GRID_H
#define GRID_H

#include "Config.h"
#include "PivImage.h"
#include <vector>

class Grid {
	
	public:
		// Constructor
		Grid(Config c, PivImage im);

		// Destructor
		// ~Grid();

		// return the x grid coordinates
		std::vector<int> getGridX();
		std::vector<int> getGridY();

		// Print vector grid size
		void printGridSize();
		void printGridPoints();

	private:
		int vectorsX;
		int vectorsY;
		std::vector<int> gridPointsX;
		std::vector<int> gridPointsY;

};

#endif // GRID_H

