#include "Grid.h"
#include "Config.h"
#include "PivImage.h"
#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

Grid::Grid(Config c, PivImage im) {

	int imW = im.getWidth();
	int imH = im.getHeight();

	vectorsX = (int) floor((imW - c.getWindowX()) / (c.getWindowX() - c.getOverlapX()) + 1);
	vectorsY = (int) floor((imH - c.getWindowY()) / (c.getWindowY() - c.getOverlapY()) + 1);
	
	for (int i = 0; i < vectorsX; i++) {
		gridPointsX.push_back((c.getWindowX() - c.getOverlapX()) * (i + 1) - (c.getWindowX() - c.getOverlapX()) + 1 + c.getWindowX() / 2);
	}

	for (int i = 0; i < vectorsY; i++) {
		gridPointsY.push_back((c.getWindowY() - c.getOverlapY()) * (i + 1) - (c.getWindowY() - c.getOverlapY()) + 1 + c.getWindowY() / 2);
	}	
}

vector<int> Grid::getGridX() {
	return gridPointsX;
}

vector<int> Grid::getGridY() {
	return gridPointsY;
}

void Grid::printGridPoints() {
	cout << "x: ";
	for (auto i : gridPointsX)
		cout << i << "\t";
	cout << endl << "y: ";
	for (auto i : gridPointsY)
		cout << i << "\t";
	cout << endl;
}

void Grid::printGridSize()  {
	cout << "Grid size: " << vectorsX << " x " << vectorsY << endl;
}