#ifndef XCORR2_H
#define XCORR2_H

#include "Config.h"
#include "Grid.h"
#include "PivImage.h"
#include <Eigen/Dense>

class XCorr2 {
	public:
		XCorr2();

		// Do the cross-correlation
		void doCC(Config c, PivImage pivIm1, PivImage pivIm2, int coordX, int coordY);

		// Find any peaks in the cross-correlation function
		

		// Return the CCF matrix
		Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> getCCFMatrix();

	private:
		Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> ccfMat;
};

#endif // XCORR_H