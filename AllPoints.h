#ifndef ALLPOINTS_H
#define ALLPOINTS_H

#include "PivPoint.h"
#include <vector>

class AllPoints {
	
	public:
		// Constructor
		AllPoints();

		// Add a point
		void addPoint(PivPoint point);

		// print points
		void printPoints();
		void printToFile();

	private:
		std::vector<PivPoint> allPoints;		
};

#endif // ALLPOINTS_H