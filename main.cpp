#include <iostream>
#include "Config.h"
#include "PivImage.h"
#include "Grid.h"
#include "XCorr2.h"
#include "PivPoint.h"
#include "Peaks.h"
#include "AllPoints.h"
#include <Eigen/Dense>

using namespace std;
using Eigen::MatrixXd;


int main() {

	cout << "BPPIV v0.0.1" << endl << endl;

	Config config;
	PivImage pivImage1("1.bmp");
	PivImage pivImage2("2.bmp");

	pivImage1.info();

	Grid g(config, pivImage1);

	AllPoints allPivData;
	PivPoint point;

	int g_i;
	int g_j;

	for (int i = 0; i < g.getGridX().size(); i++) {
		for (int j = 0; j < g.getGridY().size(); j++) {

			g_i = g.getGridX()[i];
			g_j = g.getGridY()[j];

			XCorr2 ccf;
			ccf.doCC(config, pivImage1, pivImage2, g_i, g_j);

			Peaks pks(ccf.getCCFMatrix());
			pks.findPeaks();
			//pks.printPeaks();
			pks.gaussFit();

			// Store Values
			point.setX(g_i);
			point.setY(g_j);
			point.setCCF(ccf);
			point.setU(pks.getU());
			point.setV(pks.getV());

			allPivData.addPoint(point);
		}
	}

	allPivData.printPoints();
	allPivData.printToFile();

	return 0;
}