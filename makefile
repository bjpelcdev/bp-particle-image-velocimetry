#
# Makefile for BPPIV
#

# Use GNU GCC comiler and use C++11
CC = g++
CFLAGS = -g -std=c++11

# Source files
SRCS = main.cpp AllPoints.cpp Config.cpp PivImage.cpp Grid.cpp XCorr2.cpp PivPoint.cpp Peaks.cpp

PROG = BPPIV
 
# Include third party libraries
OPENCV = `pkg-config opencv --cflags --libs`
EIGEN = `pkg-config --cflags --libs eigen3`

# Do some making 
$(PROG):$(SRCS)
	$(CC) $(CFLAGS) -o $(PROG) $(SRCS) $(OPENCV) $(EIGEN) 