	int bitSum = 0;
	double sumCor = 0;

	int coordX = g.getGridX()[1];
	int coordY = g.getGridY()[1];

	//cout << pivIm.getPixel(0, 0) << endl;
	//cout << pivIm.getPixel(31, 0) << endl;

	g.printGridPoints();

	cout << "Grid i: " << coordX << ", Grid j: " << coordY << endl;

	int m = 0;
	int n = 0;

	int i2 = 0;
	int j2 = 0;

	int ilx = 0;
	int jly = 0;

	double denom1, denom2;

	int winX = c.getWindowX();
	int winY = c.getWindowY();

	ccfMat.resize(winY, winX);

	double I1m, I2m;
	int noOverlapingPixels;

	Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> subIm;
	subIm.resize(16,16);

	for (int i = coordX - winX / 2 -1; i < coordX + winX / 2 - 1; i++) {
		for (int j = coordY - winY / 2 -1; j < coordY + winY / 2 - 1; j++) {
			//cout << "col: " << i << ", row: " << j << endl;
			//cout << "index col: " << i-(coordX - winX / 2 -1) << ", index row: " << j-(coordY - winY / 2 -1) << endl;
			subIm(j-(coordY - winY / 2 -1), i-(coordX - winX / 2 -1)) = pivIm.getPixel(j,i);
		}
	}

	cout << subIm << endl;
	

	// for each spatial offset
	for (int lagX = -winX / 2; lagX < winX / 2; lagX++) {
		for (int lagY = -winY / 2; lagY < winY / 2; lagY++) {

			// reset mean and overlapping pixel counter
			I1m = I2m = 0;
			noOverlapingPixels = 0;

			// calculate the mean of the overlapping parts
			for (int j = 0; j < winY; j++) {
				for (int i = 0; i < winX; i++ ) {
					jly = j + lagY;
					ilx = i + lagX;
					if (ilx >= 0 && ilx < winX && jly >= 0 && jly < winY) {
						noOverlapingPixels++;
						I1m += (double) pivIm.getPixel(j+(coordY - winY / 2 -1), i +(coordX - winX / 2 -1));
						I2m += (double) pivIm.getPixel(jly+(coordY - winY / 2), ilx +(coordX - winX / 2 ));
						//I1m += (double) pivIm.getPixel(j, i);
						//I2m += (double) pivIm.getPixel(jly, ilx);
					}
				}
			}

			// divide by the number of overlapping pixels to get the mean pixel values
			if (noOverlapingPixels > 0) {
				I1m /= (double) noOverlapingPixels;
				I2m /= (double) noOverlapingPixels;
			}

			// reset correlation value and denominators for each delay
			sumCor = 0;
			denom1 = 0;
			denom2 = 0;

			for (int i = coordX - winX / 2 -1; i < coordX + winX / 2 - 1; i++) {
				for (int j = coordY - winY / 2 -1; j < coordY + winY / 2 - 1; j++) {
					//cout << "Row " << j << ", col " << i << " val " << pivIm.getPixel(j, i) << endl;
					i2 = i + lagX;
					j2 = j + lagY;

					// if everything is in bounds calculate the numerator and denominator parts
					if (i2 < 0 || i2 > (winX) - 1 || j2 < 0 || j2 > (winY) - 1)
						continue;
					else {
						sumCor += ((double) pivIm.getPixel(j, i) - I1m) * ((double) pivIm.getPixel(j2, i2) - I2m);
						denom1 += ((double) pivIm.getPixel(j, i) - I1m) * ((double) pivIm.getPixel(j, i) - I1m);
						denom2 += ((double) pivIm.getPixel(j2, i2) - I2m) * ((double) pivIm.getPixel(j2, i2) - I2m);
					}
				}
			}

			// make sure we're not dividing by zero
			if (denom1 != 0 && denom2 != 0) 
			{
				ccfMat(lagY+winY/2,lagX+winX/2) = sumCor / (sqrt(denom1 * denom2));
			}
			else
			{
				ccfMat(lagY+winY/2,lagX+winX/2) = -1;
			}

		}

	}

	cout << ccfMat << endl;