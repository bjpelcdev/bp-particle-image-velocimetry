#ifndef PEAKS_H
#define PEAKS_H

#include <Eigen/Dense>
#include <vector>

class Peaks {


	struct Peak {
		int x_i, y_j;
		double peakValue;
	};

	struct Displacement {
		double u, v;
	};

	public:

		// Constructors
		Peaks();
		Peaks(Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> inputMatrix);

		// Setters
		void setMatrix(Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> inputMatrix);

		// Getters
		Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> getMatrix();
		double getU();
		double getV();

		// find maximums
		void findPeaks();

		// print peaks
		void printPeaks();

		void gaussFit();

	private:

		// Matrix of double values (ie. correlation function);
		Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> mat;

		// Store the peaks
		std::vector<Peak> peaks;

		// Store the sub-pixel displacements
		std::vector<Displacement> displacements;

		double maxPeakValue(std::vector<Peak> peakVec);
};

#endif // PEAKS_H