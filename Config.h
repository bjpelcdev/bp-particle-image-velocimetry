#ifndef CONFIG_H
#define CONFIG_H

class Config {

	public:
		// Constructors
		Config();

		// Getters
		int getWindowX();
		int getWindowY();
		int getOverlapX();
		int getOverlapY();
		double getScaleX();
		double getScaleY();
		double getDt();

		// Setters
		void setWindowX(int i);
		void setWindowY(int i);
		void setOverlapX(int i);
		void setOverlapY(int i);
		void setScaleX(double d);
		void setScaleY(double d);
		void setDt(double d);

		// Print config info
		void printConfig();


	private:
		int windowX, windowY;
		int overlapX, overlapY;
		double dt;
		double scaleX;
		double scaleY;

};

#endif // CONFIG_H