#ifndef PIVPOINT_H
#define PIVPOINT_H

//#include "Config.h"
//#include "PivImage.h"
#include "XCorr2.h"

class PivPoint {
	
	public:
		// Constructor
		PivPoint();

		// Destructor
		//~PivPoint();

		// Getters
		// Get x and y coords and u and v velocities
		int getX();
		int getY();
		double getU();
		double getV();
		XCorr2 getCCF();

		// Setters
		void setX(int x);
		void setY(int y);
		void setU(double uVel);
		void setV(double vVel);
		void setCCF(XCorr2 ccfObj);

		// Print
		void printPoint();

	private:
		// Each PIV point has the following:
		// grid coordinates in i and j
		int i_x, j_y;

		// A correlation function
		XCorr2 ccf;

		// A list of peaks?

		// Subpixel displacements
		double u, v;


};

#endif // PIVPOINT_H